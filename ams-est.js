const roster = [
    {
        heroName: "Captain America",
        secretIdentity: "Steve Rogers",
        specialty: "shields",
        isAlive: true,
    },
    {
        heroName: "Hulk",
        secretIdentity: "Bruce Banner",
        specialty: "smash",
        isAlive: true,
    },
    {
        heroName: "Iron Man",
        secretIdentity: "Tony Stark",
        specialty: "suits",
        isAlive: false,
    },
    {
        heroName: "Spider-Man",
        secretIdentity: "Peter Parker",
        specialty: "webs",
        isAlive: true,
    },
    {
        heroName: "Black Panther",
        secretIdentity: "T'challa",
        specialty: "claws",
        isAlive: true,
    },
]

/*
    1. Assemble an array of avengers objects
        a. Hardcode the data
    2. display the list of Avengers on the page
        a. loop over the array
        b. display each Avenger's data
*/
const displayHeroData = function (heroObj) {
    const itemEl = document.createElement('li')

    const nameEl = document.createTextNode("Hero: " + heroObj.heroName)
    itemEl.appendChild(nameEl)
    itemEl.appendChild(document.createElement('br'))

    const identityEl = document.createTextNode('Secret Identity: ' + heroObj.secretIdentity)
    itemEl.appendChild(identityEl)
    itemEl.appendChild(document.createElement('br'))

    const specialtyEl = document.createTextNode('Specialty: ' + heroObj.specialty)
    itemEl.appendChild(specialtyEl)
    itemEl.appendChild(document.createElement('br'))

    const aliveEl = document.createTextNode('Alive?: ' + heroObj.isAlive)
    itemEl.appendChild(aliveEl)

    listEl.appendChild(itemEl)
}

const listEl = document.querySelector('#avengersList')

for (let i=0; i<roster.length; i++ ) {
    displayHeroData(roster[i])
}