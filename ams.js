const roster = [
    {
        heroName: "Captain America",
        secretIdentity: "Steve Rogers",
        specialty: "Shields",
        damageCost: 300,
        isAlive: true 
    },
    {
        heroName: "Hulk",
        secretIdentity: "Bruce Banner",
        specialty: "Smash",
        damageCost: 1000,
        isAlive: true
    },
    {
        heroName:  "Black Panther",
        secretIdentity: "T'challa",
        specialty: "Claws",
        damageCost: 1.2,
        isAlive: true
    },
    {
        heroName: "Iron Man",
        secretIdentity: "Tony Stark",
        specialty: "Suits",
        damageCost: 850,
        isAlive: false 
    },
    {
        heroName: "Spider-Man",
        secretIdentity: "Peter Parker",
        specialty: "Webs",
        damageCost: 0.9,
        isAlive: true
    }
]


/*
    1. Assemble data on all of the Avengers
        - hardcode
    2. Display the data in the webpage
        - loop through the array of avengers
        - display details of each hero

    Refactoring
*/
const displayHero = function (heroObj) {
    const displayProperty = function (label, value, parentNode) {
        const textNode = document.createTextNode(label + ": " + value)
        parentNode.appendChild(textNode)
        parentNode.appendChild(document.createElement('br'))
    }

    const itemEl = document.createElement('li')

    displayProperty("Hero name", heroObj.heroName, itemEl)
    displayProperty("Secret Identity", heroObj.secretIdentity, itemEl)
    displayProperty("Specialty", heroObj.specialty, itemEl)
    displayProperty("Damage Cost (in millions)", heroObj.damageCost, itemEl)
    displayProperty("Alive? ", heroObj.isAlive, itemEl)

    const avengersList = document.querySelector('#avengersList')
    avengersList.appendChild(itemEl)
}

for (let i=0; i<roster.length; i++) {
    displayHero(roster[i])
}