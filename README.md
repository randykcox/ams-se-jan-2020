# Avengers Management System

Nick Fury asked us to create a web-based system to help him keep track of all of the heroes in the Avengers. This is a
quick prototype with hardcoded data that we can use to get
more feedback from Nick before we spend too much time making
a full-featured version.